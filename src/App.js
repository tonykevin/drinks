import React from 'react'
import { Form, PageHeader, RecipeList } from './components'

import CategoriesProvider from './context/CategoriesContext'
import RecipesProvider from './context/RecipesContext'
import ModalProvider from './context/ModalContext'

function App () {
  return (
    <CategoriesProvider>
      <RecipesProvider>
        <ModalProvider>
          <PageHeader />
          <div className='container mt-5'>
            <div className='row'>
              <Form />
            </div>
            <RecipeList />
          </div>
        </ModalProvider>
      </RecipesProvider>
    </CategoriesProvider>
  )
}

export default App
