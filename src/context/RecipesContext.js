import React, { createContext, useState, useEffect } from 'react'
import axios from 'axios'

export const RecipesContext = createContext()

const RecipesProvider = (props) => {
  const [recipes, setRecipes] = useState([])
  const [recipeSearch, setRecipeSearch] = useState({
    category: '',
    ingredient: ''
  })
  const [query, setQuery] = useState(false)

  const { category, ingredient } = recipeSearch

  useEffect(() => {
    if (query) {
      const getRecipes = async () => {
        const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${ingredient}&c=${category}`

        const response = await axios.get(url)
        setRecipes(response.data.drinks)
      }

      getRecipes()
    }
  }, [query, category, ingredient])

  return (
    <RecipesContext.Provider
      value={{
        recipes,
        setQuery,
        setRecipeSearch
      }}
    >
      {props.children}
    </RecipesContext.Provider>
  )
}

export default RecipesProvider
