import React, { useContext, useState } from 'react'

import { CategoriesContext } from '../context/CategoriesContext'
import { RecipesContext } from '../context/RecipesContext'

const Form = () => {
  const { categories } = useContext(CategoriesContext)
  const { setRecipeSearch, setQuery } = useContext(RecipesContext)

  const [search, setSearch] = useState({
    category: '',
    ingredient: ''
  })

  const defineSearch = ({ target }) => {
    setSearch({
      ...search,
      [target.name]: target.value
    })
  }

  return (
    <form
      className='col-12'
      onSubmit={e => {
        e.preventDefault()
        setRecipeSearch(search)
        setQuery(true)
      }}
    >
      <fieldset>
        <legend>Busca bebidas por categoría o ingrediente</legend>
      </fieldset>
      <div className='row mt-4'>
        <div className='col-md-5'>
          <input
            className='form-control'
            name='ingredient'
            onChange={defineSearch}
            placeholder='Buscar por ingrediente'
            type='text'
          />
        </div>
        <div className='col-md-5'>
          <select
            className='form-control'
            name='category'
            onChange={defineSearch}
          >
            <option value=''>-- Selecciona categoría --</option>
            {categories.map(({ strCategory }) => (
              <option
                key={strCategory}
                value={strCategory}
              >
                {strCategory}
              </option>
            ))}
          </select>
        </div>
        <div className='col-md-2'>
          <button
            className='btn btn-block btn-primary'
            type='submit'
          >
            Buscar
          </button>
        </div>
      </div>
    </form>
  )
}

export default Form
