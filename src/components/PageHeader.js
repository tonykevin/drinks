import React from 'react'

const PageHeader = () => {
  return (
    <header className='bg-alert'>
      <h1>Busca recetas de bebidas</h1>
    </header>
  )
}

export default PageHeader
