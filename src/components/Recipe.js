import React, { useContext, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Modal from '@material-ui/core/Modal'
import { ModalContext } from '../context/ModalContext'

function getModalStyle () {
  const top = 50
  const left = 50

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  }
}

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 450,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

const Recipe = ({ id, drink, drinkThumb }) => {
  const [modalStyle] = useState(getModalStyle)
  const [open, setOpen] = useState(false)

  const classes = useStyles()

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const { recipe, saveIdRecipe, setRecipe } = useContext(ModalContext)

  const showIngredients = recipe => {
    const ingredients = []

    for (let i = 1; i < 16; i++) {
      if (recipe[`strIngredient${i}`]) {
        ingredients.push(
          <li> {recipe[`strIngredient${i}`]} {recipe[`strMeasure${i}`]}</li>
        )
      }
    }

    return ingredients
  }

  return (
    <div className='col-md-4 mb-3'>
      <div className='card'>
        <h2 className='card-header'>{drink}</h2>
        <img
          className='card-img-top'
          alt={`imagen de ${drink}`}
          src={drinkThumb}
        />
        <div className='card-body'>
          <button
            type='button'
            className='btn btn-block btn-primary'
            onClick={e => {
              saveIdRecipe(id)
              handleOpen()
            }}
          >
            ver receta
          </button>
          <Modal
            open={open}
            onClose={() => {
              saveIdRecipe(null)
              setRecipe({})
              handleClose()
            }}
          >
            <div style={modalStyle} className={classes.paper}>
              <h2>{recipe.strDrink}</h2>
              <h3 className='mt-4'>Instrucciones</h3>
              <p>{recipe.strInstructions}</p>
              <img
                className='img-fluid my-4'
                alt={recipe.strDrink}
                src={recipe.strDrinkThumb}
              />
              <h3>Ingredientes y cantidades</h3>
              <ul>
                {showIngredients(recipe)}
              </ul>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  )
}

export default Recipe
