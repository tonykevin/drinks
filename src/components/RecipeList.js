import React, { useContext } from 'react'
import { RecipesContext } from '../context/RecipesContext'
import Recipe from './Recipe'

const RecipeList = () => {
  const { recipes } = useContext(RecipesContext)

  return (
    <div className='row mt-5'>
      {recipes.map(({ idDrink, strDrink, strDrinkThumb }) => (
        <Recipe
          key={idDrink}
          id={idDrink}
          drink={strDrink}
          drinkThumb={strDrinkThumb}
        />
      ))}
    </div>
  )
}

export default RecipeList
